'use strict';

var cursorDiv;

function paintCursor(left, top) {
  if (cursorDiv) {
    cursorDiv.style.webkitTransform = 'translate(' + left + 'px,' + top + 'px) translateZ(0)';
  }
}

function onMouseMove(e) {
  paintCursor(e.clientX, e.clientY);
}

function onMouseDown(e) {
  onMouseMove(e);
  cursorDiv.className = ['shopper-msp4-cursor', 'down'].join(' ');
}

function onMouseUp(e) {
  onMouseMove(e);
  cursorDiv.className = 'shopper-msp4-cursor';
}

function onMouseLeave(e) {
  if (e.target === document) {
    paintCursor(-100, -100);
  }
}

function addCursor() {
  if (!cursorDiv) {
    cursorDiv = document.createElement('div');
    cursorDiv.className = 'shopper-msp4-cursor';
    document.body.appendChild(cursorDiv);

    document.addEventListener('mousemove', onMouseMove, true);
    document.addEventListener('mousedown', onMouseDown, true);
    document.addEventListener('mouseup', onMouseUp, true);
    document.addEventListener('mouseleave', onMouseLeave, true);
  }
}

function removeCursor() {
  if (cursorDiv && cursorDiv.parentNode) {
    document.body.removeChild(cursorDiv);
    document.removeEventListener('mousemove', onMouseMove, true);
    document.removeEventListener('mousedown', onMouseDown, true);
    document.removeEventListener('mouseup', onMouseUp, true);
    document.removeEventListener('mouseleave', onMouseLeave, true);
    cursorDiv = null;
  }
}

function init() {
  if (config === 'add') {
    addCursor();
  } else if (config === 'remove') {
    removeCursor();
  }
}

init();

