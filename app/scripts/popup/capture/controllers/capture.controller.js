'use strict';

var console = chrome.extension.getBackgroundPage().console;

function CaptureController() {
  this.start = function () {
    chrome.runtime.sendMessage('start');
  };

  this.stop = function () {
    chrome.runtime.sendMessage('stop');
  };
}

CaptureController.$inject = [];
angular.module('capture').controller('CaptureController', CaptureController);