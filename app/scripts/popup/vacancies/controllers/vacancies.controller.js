'use strict';

function VacancyListCtrl(VacancyService, geolocation) {
  var self = this;

  this.filter = {
    internet: true,
    phone: true,
    visit: true,
    range: 20,
    reward: 0,
    spheres: [1, 2, 3, 4, 5, 6, 35, 7, 8, 12, 9, 11, 13, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 31, 28, 29, 30, 31]
  };

  geolocation.getLocation({maximumAge: Infinity})
    .then(function (data) {
      return [data.coords.latitude, data.coords.longitude];
    })
    .then(function (coords) {
      self.filter.coordinates = coords.join(',');
      // Поиск вакансий по критериям агента
      VacancyService.getList(self.filter.coordinates).then(function (data) {
        self.vacancies = data;
        chrome.browserAction.setBadgeText({text: data.length.toString()});
      });
    });

  this.openWebsite = function () {
    chrome.tabs.create({url: 'http://www.msp4.me'});
  };

}

VacancyListCtrl.$inject = ['VacancyService', 'geolocation'];

angular
  .module('vacancies')
  .controller('VacancyListCtrl', VacancyListCtrl);
