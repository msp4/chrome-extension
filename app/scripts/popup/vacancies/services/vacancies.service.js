'use strict';

function VacancyService() {
  this.$get = function ($q, $http) {
    return {
      /**
       * Get vacancy list from the API
       * @param params
       * @returns {promise|*|qFactory.Deferred.promise|dd.g.promise}
       */
      getList: function (params) {
        var d = $q.defer();

        // call API
        $http.get('http://client.msp4.me/api/vacancies/search?coordinates=' + params)
          .success(function (data) {
            d.resolve(data);
          }).error(function (err) {
            d.reject(err);
          });

        return d.promise;
      }
    };
  };
}

angular
  .module('vacancies')
  .provider('VacancyService', VacancyService);

