'use strict';

function Authentication($q) {
  var d = $q.defer();

  chrome.runtime.getBackgroundPage(function (window) {
    d.resolve(window.user);
  });

  return d.promise;
}

Authentication.$inject = ['$q'];
angular.module('users').factory('Authentication', Authentication);