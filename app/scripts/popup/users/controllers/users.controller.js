'use strict';

function UsersController(Authentication, constant, $window) {
  var self = this;
  Authentication.then(function (user) {
    self.user = user;
  });

  this.signup = function () {
    chrome.tabs.update(null, { url: 'http://www.msp4.me'}, function () {
      $window.close();
    });
  };

  this.signin = function () {
    chrome.tabs.update(null, {url: constant.SHOPPER_URL + '#!/signin'}, function () {
      $window.close();
    });

  }
}

UsersController.$inject = ['Authentication', 'constant', '$window'];
angular.module('users').controller('UsersController', UsersController);