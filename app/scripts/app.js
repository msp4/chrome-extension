'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

window.NODE_ENV = 'development';

// Setting constant
angular.module(ApplicationConfiguration.applicationModuleName).constant('constant', {
  CLIENT_API: window.NODE_ENV !== 'production' ? 'http://localhost:3001/api/' : 'http://client.msp4.me/api/',
  SHOPPER_URL: window.NODE_ENV !== 'production' ? 'http://localhost:3000/' : 'http://shopper.msp4.me/'
});

//Then define the init function for starting up the application
angular.element(document).ready(function() {

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});