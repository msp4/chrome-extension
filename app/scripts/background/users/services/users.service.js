'use strict';

function Users($http, constant) {
  var user = {};

  user.getMeProfile = function () {
    return $http.get(constant.SHOPPER_URL + 'users/me');
  };

  return user;
}

Users.$inject = ['$http', 'constant'];
angular.module('users').service('Users', Users);