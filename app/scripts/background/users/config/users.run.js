'use strict';

function UsersRun(Users, $window, constant) {
  var isUpdate = false;

  // Получаем пользователя
  function getUser() {
    isUpdate = true;
    Users.getMeProfile().then(function (response) {
      isUpdate = false;
      $window.user = response.data;
    });
  }

  getUser();

  var lengthShopperUrl = constant.SHOPPER_URL.length;
  chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tabInfo) {
    if ((changeInfo.status === 'complete') && ((tabInfo.url.substring(0, lengthShopperUrl) === constant.SHOPPER_URL) || (tabInfo.url.substring(0, 18) === 'http://www.msp4.me'))) {
      if (!isUpdate) getUser();
    }
  });
}

UsersRun.$inject = ['Users', '$window', 'constant'];
angular.module('users').run(UsersRun);