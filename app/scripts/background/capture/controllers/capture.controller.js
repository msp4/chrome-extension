'use strict';

function CaptureController(azureBlob) {
  var mediaRecorder,
    localStream,
    tabId;

  function gotStream(stream) {
    if (!stream) {
      console.error('Unable to capture the tab. Note that Chrome internal pages cannot be captured.');
      return null;
    }
    console.log('Start stream!');
    localStream = stream;


    mediaRecorder = new MediaStreamRecorder(stream);
    mediaRecorder.mimeType = 'video/webm';
    mediaRecorder.width = 1280;
    mediaRecorder.height = 720;


    mediaRecorder.ondataavailable = function (blob) {
      console.log('Stop stream!');
      azureBlob.upload({
        baseUrl: 'https://msp4research.blob.core.windows.net:443/543e7c57a6e3603c1ceb2e4f/54913bfbb12db738149010d6/0/0/0/0',
        file: blob,
        sasToken: '?' + 'st=2015-01-20T14%3A30%3A26Z&se=2016-01-15T14%3A30%3A26Z&sp=rw&sv=2014-02-14&sr=b&sig=tw3QVX6ohbVSwwxx3jBEWmiRrw7bnAOpbYyDNVnRU2c%3D',
        progress: function (percent) {

        },
        complete: function () {
          console.log('Complete');
        },
        error: function (err) {
          console.error('Загрузка не удалась ' + err);
        }
      });
    };

    mediaRecorder.start(1200000);
  }

  function startStream() {
    chrome.tabs.getSelected(null, function (tab) {
      var MediaStreamConstraint = {
        audio: false,
        video: true,
        videoConstraints: {
          mandatory: {
            chromeMediaSource: 'tab',
            minWidth: 640,
            minHeight: 480,

            maxWidth: 1920,
            maxHeight: 1080,

            minAspectRatio: 1.77
          }
        }
      };
      tabId = tab.id;

      chrome.tabCapture.capture(MediaStreamConstraint, gotStream);
      chrome.tabs.insertCSS(tabId, {file: 'styles/cursor.css'});
      chrome.tabs.executeScript(tabId, {code: 'var config=\'add\';'}, function () {
        chrome.tabs.executeScript(tabId, {file: 'scripts/cursor.js'});
      });
    });
  }

  function stop() {
    localStream.stop();
    mediaRecorder.stop();
    chrome.tabs.executeScript(tabId, {code: 'var config=\'remove\';'}, function () {
      chrome.tabs.executeScript(tabId, {file: 'scripts/cursor.js'});
    });
  }

  chrome.runtime.onMessage.addListener(function (message, sender, callback) {
    if (message === 'start') {
      startStream();
    } else if (message === 'stop') {
      stop();
    }
  });
}

CaptureController.$inject = ['azureBlob'];
angular.module('background').controller('CaptureController', CaptureController);